# Installation

- Clone this project to your directory
- Open terminal, go to your project directory
- Run command `yarn` to install dependencies
- After that, just run `yarn start`, open brower and go to `http://localhost:3000`
