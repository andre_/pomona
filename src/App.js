import React from "react";
import "./styles/index.scss";
import Api from "./api";

function Select(props) {
  const [menu, setMenu] = React.useState(false);

  return (
    <>
      <div className="appearance-none w-full bg-gray-100 p-2 mb-4 pb-2">
        <p className="text-sm cursor-pointer" onClick={() => setMenu(!menu)}>
          {props.title}
        </p>
        {props.size ? (
          <p className="text-xs text-gray-500">Filtered</p>
        ) : (
          <p className="text-xs text-gray-500">{props.subtitle}</p>
        )}
        {menu && (
          <div className="mt-4">
            {props.data.map((v, k) => (
              <div
                key={k}
                className="text-xs text-gray-800 mt-4 flex flex-row items-center"
              >
                <input
                  type="checkbox"
                  className="mr-2 leading-tight"
                  onClick={() => props.onClick(v)}
                />
                <p>{v}</p>
              </div>
            ))}
          </div>
        )}
      </div>
    </>
  );
}

function Content(props) {
  return (
    <>
      {(props.data || []).length
        ? props.data.map((v, k) => (
            <div
              className="md:w-48 sm:w-full bg-gray-700 mr-2 mb-2 p-2 text-white"
              key={k}
            >
              <p className="font-bold mb-2">{v.name}</p>
              <p className="font-light h-32 overflow-auto">
                {(v.description || []).length ? (
                  v.description
                ) : (
                  <i>No description</i>
                )}
              </p>

              <div className="flex w-full justify-between mt-4">
                <p className="font-bold">{v.brand}</p>
                <p>{v.release_year}</p>
              </div>
            </div>
          ))
        : null}
    </>
  );
}

function App() {
  const [checkboxData] = React.useState(new Set());
  const [brandData] = React.useState(new Set());

  // const yearData = [2017, 2016, 2015, 2014, 2013];
  // const brand = ["Samsung", "Apple", "Lenovo", "Xiaomi", "LG"];

  const [{ phoneTemp, filtered, brand, year }, dispatch] = React.useReducer(
    (state, action) => {
      switch (action.type) {
        case "ADD": {
          return {
            ...state,
            phone: action.data.phone,
            phoneTemp: action.data.phone,
            brand: action.data.brand,
            year: action.data.year,
            filtered: []
          };
        }

        case "SEARCH": {
          let temp = state.phone;
          temp = temp.filter(
            v => v.name.toLowerCase().search(action.data.toLowerCase()) !== -1
          );

          return {
            ...state,
            phoneTemp: temp
          };
        }

        case "FILTER_DATE_ACTIVE": {
          let temp = state.filtered.length ? state.filtered : state.phone;
          temp = temp.filter(v => v.release_year === action.data);

          return {
            ...state,
            filtered: [...state.filtered, ...temp]
          };
        }

        case "FILTER_DATE_DEACTIVE": {
          let temp = state.filtered;
          temp = temp.filter(v => v.release_year !== action.data);

          return {
            ...state,
            filtered: temp
          };
        }

        case "FILTER_BRAND_ACTIVE": {
          let temp = state.filtered.length ? state.filtered : state.phone;
          temp = temp.filter(v => v.brand === action.data);

          return {
            ...state,
            filtered: temp
          };
        }

        case "FILTER_BRAND_DEACTIVE": {
          let temp = state.filtered;
          temp = temp.filter(v => v.brand !== action.data);

          return {
            ...state,
            filtered: temp
          };
        }

        default: {
          return state;
        }
      }
    },
    [{ phone: [], phoneTemp: [], filtered: [], brand: [], year: [] }]
  );

  React.useEffect(() => {
    Api.init().then(response => {
      let collectYear = [
        ...new Set(response.data.phones.map(v => v.release_year))
      ];
      let brand = [...new Set(response.data.phones.map(v => v.brand))];

      dispatch({
        type: "ADD",
        data: { phone: response.data.phones, year: collectYear, brand }
      });
    });
  }, []);

  const searchPhone = text => {
    dispatch({ type: "SEARCH", data: text });
  };

  const toggleCheckbox = label => {
    if (checkboxData.has(label)) {
      checkboxData.delete(label);
      dispatch({ type: "FILTER_DATE_DEACTIVE", data: label });
    } else {
      checkboxData.add(label);
      dispatch({ type: "FILTER_DATE_ACTIVE", data: label });
    }
  };

  const toggleBrand = label => {
    if (brandData.has(label)) {
      brandData.delete(label);
      dispatch({ type: "FILTER_BRAND_DEACTIVE", data: label });
    } else {
      brandData.add(label);
      dispatch({ type: "FILTER_BRAND_ACTIVE", data: label });
    }
  };

  return (
    <div className="container flex justify-center">
      <div className="w-1/2 flex flex-col self-center mt-10 bg-white p-5 shadow-md rounded-sm">
        <input
          placeholder="Search phone"
          className="w-full appearance-none border-2 border-gray-100 p-2 focus:outline-none focus:border-blue-300"
          onChange={e => searchPhone(e.target.value)}
        />

        <div className="w-full mt-6 mb-2 flex md:flex-row sm:flex-col">
          <div className="sm:w-full md:w-1/2 pr-4">
            <Select
              title="Select years"
              subtitle="All years"
              data={year}
              size={checkboxData.size}
              onClick={toggleCheckbox}
            />
          </div>
          <div className="sm:w-full md:w-1/2 pr-4">
            <Select
              title="Select phone"
              data={brand}
              subtitle="All years"
              onClick={toggleBrand}
            />
          </div>
        </div>

        <div className="w-full flex flex-wrap">
          {(filtered || []).length ? (
            <Content data={filtered} />
          ) : (
            <Content data={phoneTemp} />
          )}
        </div>
      </div>
    </div>
  );
}

export default App;
