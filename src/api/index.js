const response = require("./data");

class Api {
  static init() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        return resolve({
          success: true,
          data: response
        });
      }, 1000);
    });
  }
}

export default Api;
